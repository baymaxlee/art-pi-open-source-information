# ART-Pi 开源信息

#### 介绍
本项目的制作由本人制作并借鉴相关大牛的文章和资料制作而成，应该没啥侵权问题，如有侵权请联系本人进行删除，由此因起法律纠纷本人概不负责（保险起见添加）。
#### 项目介绍

本项目是完全使用 LCEDA 进行设计的，并参考 ART-Pi 和 野火的 GT917S 的液晶屏幕的引脚定义设计的

本人使用的物料也是直接在立创商城购买的，排母（商城编号：C50982）、FFC/FPC 连接器（商城编号：C11097）

 **注：** 排母焊接在板子的背面(如第一版实物图)

#### 使用说明

1.  打开立创 EDA 编辑器 https://lceda.cn/
2.  建立自己的项目进行设计（本人没有使用等长走线，有耐心的可以玩玩）
3.  如下是本人创建的ART-Pi野火原子的转接板示例
4.  根据如下原理图截图或者文件中的原理图等信息可自己画一个更美观的吆
5.  立创 5元 打板的尺寸为 50 mm * 50 mm 详细的以立创官网为准^_^
6.  [原始工程](https://lceda.cn/editor#id=6455b8ea74884bbb8e1230d7398b9cff|83333d31e7674d418beab211954827ba|2c5852b53e4b4e58b38becd6193cfc53|86322cba5f544208a9634d143759472d|6a2e240c068c4cba88f06685331d1380)
7.  [ART-Pi 开源硬件社区有最新版本](https://lceda.cn/ART-Pi-kai-yuan-kuo-zhan-ban)

![输入图片说明](https://images.gitee.com/uploads/images/2020/1118/130639_204dc65c_7790385.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1118/130656_25af790e_7790385.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/175454_026c7f2c_7790385.png "实物图05.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/175516_41575123_7790385.png "实物图04.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1118/131601_1b4345c4_7790385.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1118/131622_4f6bd52a_7790385.png "屏幕截图.png")

#### 参与贡献

1.  本文参考了网友的一些案例和 Bilibili 老师
2.  引脚分布参考了原子野火 ART-Pi 的原理图设计




